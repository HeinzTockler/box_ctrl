#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <Wire.h>
#include <Adafruit_MCP4725.h>
/*
   понедельник это 2
*/
/////////////Вафля///////////////////////////////////////////////////////////////
const char* ssid = "Box";
const char* password = "heinztockler";

//////////Настройки MQTT//////////////////////////////////////////////////////////
const char *mqtt_server = "192.168.0.4"; // Имя сервера MQTT
const int mqtt_port = 1883; // Порт для подключения к серверу MQTT
const char *mqtt_user = ""; // Логи для подключения к серверу MQTT
const char *mqtt_pass = ""; // Пароль для подключения к серверу MQTT

WiFiClient wclient;
PubSubClient client(wclient);
Adafruit_MCP4725 MCP4725;

/////////Сигнатуры///////////////////////////////////////////////////////////

///////Переменные/////////////////////////////////////////////////////////////
uint64_t curTime = 0; //для организации циклов прерывания
uint64_t curTimeSync = 0; //для организации циклов прерывания
uint64_t lastReconnectAttempt = 0;
uint32_t intervalUpdate = 300;
uint32_t interval = 3000; //основной цикл
uint32_t range = 0;
uint8_t level = 0;
bool debug = true;
float voltage[8] = {0.625, 1.25, 1.87, 2.5, 3.125, 3.75, 4.375, 5};

//////Сигналы MQTT//////////////////////////////////////////////////////////////////////
//bool lightOn = false;
//bool armed = false;
//bool hostIn = false;

bool door_flag = false;
//bool rightdoor = false;
//bool middledoor = false;
//bool pirStatus = false;
//////Входа//////////////////////////////////////////////////////////////////////

uint8_t buz = 13;
uint8_t trig = 14;
uint8_t echo = 12;
uint8_t door = 4;
//uint8_t pir = 0;


bool wifi_status() {
  if (WiFi.status() == WL_CONNECTED) return true;
  else return false;
}

void wifi_update() {
  if (debug) Serial.print("Wifi_update");
  if (!wifi_status()) {
    byte i = 0;
    Serial.print("Connecting to ");
    Serial.println(ssid);
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password);
    //пробуем подконектится 10 раз с интервалом в 3 сек.
    while (i < 4 && !wifi_status()) {
      Serial.print("_");
      Serial.print(i);
      delay(3000);
      i++;
    }
    Serial.println();
    if (wifi_status()) {
      Serial.println("WiFi connected");
      Serial.println("IP address: ");
      Serial.println(WiFi.localIP());
    } else Serial.println("Sync stopped, wifi is not found");
  }   else if (debug) Serial.print("..ok");
  if (debug) Serial.println("");
}

bool reconnect() {
  if (debug) Serial.println("Reconnect");
  if (client.connect("ESP_ParkL", mqtt_user, mqtt_pass)) {
    if (debug) Serial.println("MQQT Connection UP");
    client.publish("box/rdoor", "");
    // client.publish("box/ldoor", "");
    /*
       client.publish("flower_k/LastTime", "");
        client.subscribe("pump/manual");
        client.subscribe("pump/timeav");
        client.subscribe("pump/timebr");
        client.subscribe("pump/timelas");
        client.subscribe("pump/timeblab");
        client.subscribe("pump/setTime");
    */
  }
  return client.connected();
}

void mqqt_check() {
  if (debug) Serial.print("MQQT");
  if (!client.connected()) {
    if (millis() - lastReconnectAttempt > 5000) {
      if (debug) Serial.println(millis());
      lastReconnectAttempt = millis();
      if (reconnect()) {
        lastReconnectAttempt = 0;
      }
    }
    if (debug) Serial.print(" reconnect");
  } else {
    client.loop();
    if (debug) Serial.print(" loop");
  }
  if (debug) Serial.println("");
}

void callback(char* topic, byte* payload, unsigned int length) {
  String str;
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");

  for (int i = 0; i < length; i++) {
    if (debug) Serial.print((char)payload[i]);
    str += (char)payload[i];
  }
  if (debug) Serial.println();

  if ((String)topic == "box/inputLight") {
    if (debug)Serial.println(str);
    // if (str == "true")lightOn = true; else lightOn = false;
  }

}

void get_distance() {
  digitalWrite(trig, HIGH);
  delayMicroseconds(10);
  digitalWrite(trig, LOW);
  int distance = pulseIn(echo, HIGH, 10000); // возможно нужно больше 10к...
  if (distance == 0)  {
    pinMode(echo, OUTPUT);
    digitalWrite(echo, 0);
    pinMode(echo, INPUT);
    Serial.println("Out of range!");
  } else {
    range = distance / 58;
  }
  delay(10);
  if (debug)Serial.print("range= ");
  if (debug)Serial.print(range);
}


void AnalyzeRange() {
  get_distance();
  if (range > 30 || range < 0)level = 0;
  else if (range > 25 & range < 30) level = 1;
  else if (range > 20 & range < 25) level = 2;
  else if (range > 15 & range < 20) level = 3;
  else if (range > 10 & range < 15) level = 4;
  else if (range > 5 & range < 10) level = 5;
  else if (range > 3 & range < 5) level = 6;
  else if (range > 0 & range < 3) level = 7;
  if (debug)Serial.print("level= ");
  if (debug)Serial.println(level);
}

void select_level() {
  //if (debug) level=5;
  switch (level) {
    case 1:
      led_clear();
      MCP4725.setVoltage(getACP(voltage[level]), false);
      buzzer(false, 0);
      break;
    case 2:
      led_clear();
      MCP4725.setVoltage(getACP(voltage[level]), false);
      buzzer(false, 0);
      break;
    case 3:
      led_clear();
      MCP4725.setVoltage(getACP(voltage[level]), false);
      buzzer(false, 0);
      break;
    case 4:
      led_clear();
      MCP4725.setVoltage(getACP(voltage[level]), false);
      buzzer(false, 0);
      break;
    case 5:
      led_clear();
      MCP4725.setVoltage(getACP(voltage[level]), false);
      buzzer(false, 0);
      break;
    case 6:
      led_clear();
      MCP4725.setVoltage(getACP(voltage[level]), false);
      buzzer(true, 500);
      break;
    case 7:
      led_clear();
      MCP4725.setVoltage(getACP(voltage[level]), false);
      buzzer(true, 1000);
      break;
    case 0:
      led_clear();
      buzzer(false, 0);
      break;
  }
}

void led_clear() {
  MCP4725.setVoltage(getACP(0), false);
}


uint32_t getACP(float volt) {
  float MCP4725_value = (4096.0 / 5.0 ) * volt;
  if (debug) Serial.println(MCP4725_value);
  return MCP4725_value;
}

void buzzer(bool on_c, int freq) {
  if (on_c) tone (buz, freq);
  else noTone(buz);
}

void door_check() {
  char message[10];
  String str;

  if (digitalRead(door)) {
    door_flag = true;
    str = "true";
  }
  else {
    door_flag = false;
    str = "false";
  }

  str.toCharArray(message, 10);
  if (debug) Serial.println(message);
  client.publish("box/rdoor", message);
}

void setup()
{
  Serial.begin(115200);

  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);

  pinMode(buz, OUTPUT);
  pinMode(trig, OUTPUT);
  pinMode(echo, INPUT);
  pinMode(door, INPUT);
  MCP4725.begin(0x60);

 // wifi_update();
  //mqqt_check();

  curTime = millis();
  curTimeSync = millis();

  // setSyncInterval(2592000);
  // setSyncProvider(getNtpTime);
  if (debug) Serial.println("Setup_end");
}

void loop()
{
  if (millis() > (curTime + interval)) {
    curTime = millis();
    if (debug)Serial.println("======================================================================");
   // wifi_update();
    door_check();
  } else if (curTime > millis()) {
    curTime = millis();
    if (debug) Serial.println("Overflov");
  }

  if (millis() > (curTimeSync + intervalUpdate)) {
    curTimeSync = millis();
    if (debug)Serial.println("---------------------------------------------------------------------");
    if (door_flag) {
      AnalyzeRange();
      select_level();
    } else {
      led_clear();
      buzzer(false, 0);
    }
   // mqqt_check();
  } else if (curTimeSync > millis()) {
    curTimeSync = millis();
    if (debug) Serial.println("Overflov");
  }
}
