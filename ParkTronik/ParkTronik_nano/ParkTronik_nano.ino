//#include <Wire.h>
//#include <Adafruit_MCP4725.h>

#define TRIG_PIN 13
#define ECHO_PIN 12
//#define LED 13
#define buz 11
#define but_on 9

#define out7 8
#define out6 7
#define out5 6
#define out4 5
#define out3 4
#define out2 3
#define out1 2

byte out = 0;
int range = 0;
byte level = 0;
bool debug = false;

void setup() {
  //DDRB=0b00101001;
  //DDRD=0b11111100;
  pinMode(out1, OUTPUT);
  pinMode(out2, OUTPUT);
  pinMode(out3, OUTPUT);
  pinMode(out4, OUTPUT);
  pinMode(out5, OUTPUT);
  pinMode(out6, OUTPUT);
  pinMode(out7, OUTPUT);

  pinMode(TRIG_PIN, OUTPUT);
  pinMode(ECHO_PIN, INPUT);

  pinMode(buz, OUTPUT);
  pinMode(but_on, INPUT);

  delay(1000);
  Serial.begin(9600);
}

void loop() {
  //  get_distance();
  if (digitalRead(but_on)) {
    AnalyzeRange();
    select_level();
  } else {
    if (debug)Serial.println("Parktronik_0ff");
    delay(3000);
  }
}

void get_distance() {
  digitalWrite(TRIG_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN, LOW);
  int distance = pulseIn(ECHO_PIN, HIGH, 10000); // возможно нужно больше 10к...
  if (distance == 0)  {
    pinMode(ECHO_PIN, OUTPUT);
    digitalWrite(ECHO_PIN, 0);
    pinMode(ECHO_PIN, INPUT);
    Serial.println("Out of range!");
  } else {
    range = distance / 58;
  }
  delay(100);
  if (debug)Serial.print("range= ");
  if (debug)Serial.print(range);
}

void AnalyzeRange() {
  get_distance();
  if (range > 30 || range < 0)level = 0;
  else if (range > 25 & range < 30) level = 1;
  else if (range > 20 & range < 25) level = 2;
  else if (range > 15 & range < 20) level = 3;
  else if (range > 10 & range < 15) level = 4;
  else if (range > 5 & range < 10) level = 5;
  else if (range > 3 & range < 5) level = 6;
  else if (range > 0 & range < 3) level = 7;
  if (debug)Serial.print("level= ");
  if (debug)Serial.println(level);
}

void select_level() {
  //if (debug) level=5;
  switch (level) {
    case 1:
      led_clear();
      digitalWrite(out1, HIGH);
      buzzer(false, 0);
      break;
    case 2:
      led_clear();
      digitalWrite(out2, HIGH);
      buzzer(false, 0);
      break;
    case 3:
      led_clear();
      digitalWrite(out3, HIGH);
      buzzer(false, 0);
      break;
    case 4:
      led_clear();
      digitalWrite(out4, HIGH);
      buzzer(false, 0);
      break;
    case 5:
      led_clear();
      digitalWrite(out5, HIGH);
      buzzer(false, 0);
      break;
    case 6:
      led_clear();
      digitalWrite(out6, HIGH);
      buzzer(true, 500);
      break;
    case 7:
      led_clear();
      digitalWrite(out7, HIGH);
      buzzer(true, 1000);
      break;
    case 0:
      led_clear();
      buzzer(false, 0);
      break;
  }
}

void led_clear() {
  digitalWrite(out1, LOW);
  digitalWrite(out2, LOW);
  digitalWrite(out3, LOW);
  digitalWrite(out4, LOW);
  digitalWrite(out5, LOW);
  digitalWrite(out6, LOW);
  digitalWrite(out7, LOW);
}

void buzzer(bool on, int freq) {
  if (on) tone (buz, freq);
  else noTone(buz);//включаем на 500 Гц
  // delay(100); //ждем 100 Мс
  //tone(buz, 1000); //включаем на 1000 Гц
  //delay(100); //ждем 100 Мс
}

