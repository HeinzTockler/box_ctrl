/*
  pinMode()
  digitalWrite()
  digitalRead()
  analogRead()
  analogReference(INTERNAL) / (EXTERNAL)
  shiftOut()
  pulseIn()
  analogWrite()
  millis()
  micros()
  delay()
  delayMicroseconds()
*/
#include <Wire.h>
#include <Adafruit_MCP4725.h>

#define TRIG_PIN 1
#define ECHO_PIN 3
#define ZUM 4

int out = 0;
int range = 0;

Adafruit_MCP4725 dac;

void setup() {
  pinMode(ZUM, OUTPUT);
  pinMode(TRIG_PIN, OUTPUT);
  pinMode(ECHO_PIN, INPUT);
  analogWrite(ZUM, 255);
  dac.begin(0x62);
  delay(1000);
}

void loop() {
  get_distance();
  if (range <= 30) {
    out = maped(range, 30, 0, 0, 4096);
    dac.setVoltage(out, false);
  }
}

void get_distance() {
  digitalWrite(TRIG_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN, LOW);
  int distance = pulseIn(ECHO_PIN, HIGH, 10000); // возможно нужно больше 10к...
  if (distance == 0)  {
    pinMode(ECHO_PIN, OUTPUT);
    digitalWrite(ECHO_PIN, 0);
    pinMode(ECHO_PIN, INPUT);
  } else {
    range = distance / 58;
  }
  delay(400);
}

long maped(long x, long in_min, long in_max, long out_min, long out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

