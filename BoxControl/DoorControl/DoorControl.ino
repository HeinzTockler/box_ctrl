#include <ESP8266WiFi.h>
#include <PubSubClient.h>
/*
   понедельник это 2
*/
/////////////Вафля///////////////////////////////////////////////////////////////
const char* ssid = "Redmi";
const char* password = "192422901";
/*
  const char* ssid = "Box";
  const char* password = "heinztockler";
*/
//////////Настройки MQTT//////////////////////////////////////////////////////////
const char *mqtt_server = "m23.cloudmqtt.com"; // Имя сервера MQTT
const int mqtt_port = 18432; // Порт для подключения к серверу MQTT
const char *mqtt_user = "pkwbulhk"; // Логи для подключения к серверу MQTT
const char *mqtt_pass = "tvq54erCELYp"; // Пароль для подключения к серверу MQTT
/*
   const char *mqtt_server = "192.168.11.4"; // Имя сервера MQTT
  const int mqtt_port = 1883; // Порт для подключения к серверу MQTT
  const char *mqtt_user = "heinz"; // Логи для подключения к серверу MQTT
  const char *mqtt_pass = "192422901"; // Пароль для подключения к серверу MQTT
*/
WiFiClient wclient;
PubSubClient client(wclient);

/////////Сигнатуры///////////////////////////////////////////////////////////

///////Переменные/////////////////////////////////////////////////////////////
uint64_t curTime = 0; //для организации циклов прерывания
uint64_t curTimeUp = 0; //для организации циклов прерывания
uint64_t lastReconnectAttempt = 0;
//uint32_t intervalUpdate = 300;
//uint32_t interval = 400; //основной цикл
uint32_t intervalUpdate = 1000;
uint32_t interval = 1200; //основной цикл
bool debug = true;

//для приема сигналов
bool lightOn = false;
//bool leftlightOn = false;
//bool rightlightOn = false;
bool armed = false;
bool hostIn = false;

bool leftdoor = false;
bool rightdoor = false;
bool middledoor = false;
bool pirStatus = false;

//////Входа//////////////////////////////////////////////////////////////////////
uint8_t left = 4;
uint8_t right = 5;
uint8_t light = 13;
uint8_t trig = 14;
uint8_t echo = 12;
uint8_t pir = 0;

bool wifi_status() {
  if (WiFi.status() == WL_CONNECTED) return true;
  else return false;
}


void wifi_update() {
  if (debug) Serial.print("Wifi_update");
  if (!wifi_status()) {
    byte i = 0;
    Serial.print("Connecting to ");
    Serial.println(ssid);
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password);
    //пробуем подконектится 10 раз с интервалом в 3 сек.
    while (i < 4 && !wifi_status()) {
      Serial.print("_");
      Serial.print(i);
      delay(3000);
      i++;
    }
    Serial.println();
    if (wifi_status()) {
      Serial.println("WiFi connected");
      Serial.println("IP address: ");
      Serial.println(WiFi.localIP());
    } else Serial.println("Sync stopped, wifi is not found");
  }   else if (debug) Serial.print("..ok");
  if (debug) Serial.println("");
}

bool reconnect() {
  if (debug) Serial.println("Reconnect");
  if (client.connect("ESP_Door", mqtt_user, mqtt_pass)) {
    if (debug) Serial.println("MQQT Connection UP");
    client.subscribe("box/inputLight");
    //client.subscribe("box/leftLight");
    //client.subscribe("box/rightLight");
    client.subscribe("box/hostIn");
    client.subscribe("box/armed");
  }
  return client.connected();
}

void mqqt_check() {
  if (debug) Serial.print("MQQT");
  if (!client.connected()) {
    if (millis() - lastReconnectAttempt > 5000) {
      if (debug) Serial.println(millis());
      lastReconnectAttempt = millis();
      if (reconnect()) {
        lastReconnectAttempt = 0;
      }
    }
    if (debug) Serial.print(" reconnect");
  } else {
    client.loop();
    if (debug) Serial.print(" loop");
  }
  if (debug) Serial.println("");
}

void callback(char* topic, byte* payload, unsigned int length) {
  String str;
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");

  for (int i = 0; i < length; i++) {
    if (debug) Serial.print((char)payload[i]);
    str += (char)payload[i];
  }
  if (debug) Serial.println();

  if ((String)topic == "box/inputLight") {
    if (debug)Serial.println(str);
    if (str == "true")lightOn = true; else lightOn = false;
  }


  if ((String)topic == "box/armed") {
    if (debug)Serial.println(str);
    if (str == "true")armed = true; else armed = false;
    if (hostIn) {
      hostIn = !hostIn;
      if (debug)Serial.println("hostIn inverted");
    }
  }


  if ((String)topic == "box/hostIn") {
    if (debug)Serial.println(str);
    if (str == "true")hostIn = true; else hostIn = false;
    if (armed) {
      armed = !armed;
      if (debug)Serial.println("armed inverted");
    }
  }
}

void left_door_status(bool stat) {
  if (leftdoor != stat) {
    if (stat)client.publish("box/ldoor", "true"); else client.publish("box/ldoor", "false");
    leftdoor = stat;
  }
}

void right_door_status(bool stat) {
  if (rightdoor != stat) {
    if (stat)client.publish("box/rdoor", "true"); else client.publish("box/rdoor", "false");
    rightdoor = stat;
  }
}

void small_door_status(bool stat) {
  if (middledoor != stat) {
    if (stat)client.publish("box/mdoor", "true"); else client.publish("box/mdoor", "false");
    middledoor = stat;
  }
}

void pir_Input_status(bool stat) {
  if (pirStatus != stat) {
    if (stat)client.publish("box/pirInput", "true"); else client.publish("box/pirInput", "false");
    pirStatus = stat;
  }
}


void DoorChek() {
  uint32_t temp = 0;
  if (digitalRead(right)) {
    right_door_status(true);
    if (debug)Serial.println("right");
  } else {
    right_door_status(false);
  }

  if (digitalRead(left)) {
    left_door_status(true);
    if (debug)Serial.println("left");
  } else {
    left_door_status(false);
  }

  temp = get_distance();
 // temp = 0;
  if (temp >= 2) {
    small_door_status(true);
    if (debug)Serial.println("midle");
  } else {
    small_door_status(false);
  }

  if (analogRead(pir) <= 512) {
    pir_Input_status(false);
    if (debug)Serial.println("pir");
  } else {
    pir_Input_status(true);
  }
}

uint32_t get_distance() {
  uint32_t distance = 0;
  uint32_t range = 0;
  digitalWrite(trig, HIGH);
  delayMicroseconds(10);
  digitalWrite(trig, LOW);
  distance = pulseIn(echo, HIGH, 10000); // возможно нужно больше 10к...
  if (distance == 0)  {
    pinMode(echo, OUTPUT);
    digitalWrite(echo, 0);
    pinMode(echo, INPUT);
    if (debug)Serial.println("Out of range!");
  } else {
    range = distance / 58;
  }
  delay(10);
  if (debug)Serial.print("range= ");
  if (debug)Serial.print(range);
  return range;
}

void lightCtrl() {
  //гараж на охране, включаем свет если открылась дверь
  if (armed) {
    //дверь открыта и гараж на охране
    if (middledoor || pirStatus)lightOn = true;
    // digitalWrite(light, HIGH);
    //  else digitalWrite(light, LOW);
  } else {
    //пользователь авторизован и есть команда на включение
    if (lightOn) digitalWrite(light, HIGH);
    else digitalWrite(light, LOW);
  }
}

void setup()
{
  Serial.begin(115200);

  pinMode(left, INPUT);
  pinMode(right, INPUT);
  pinMode(light, OUTPUT);
  pinMode(trig, OUTPUT);
  pinMode(echo, INPUT);
  pinMode(pir, INPUT);

  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);

  wifi_update();
  mqqt_check();

  curTime = millis();
  curTimeUp = millis();
  if (debug) Serial.println("Setup_end");
}

void loop()
{
  if (millis() > (curTime + intervalUpdate)) {
    curTime = millis();
    if (debug)Serial.println("======================================================================");
    wifi_update();
    mqqt_check();
  } else if (curTime > millis()) {
    curTime = millis();
    Serial.println("Overflov");
  }

  if (millis() > (curTimeUp + interval)) {
    curTimeUp = millis();
    if (debug)Serial.println("---------------------------------------------------------------------");
    DoorChek();
    lightCtrl();
  } else if (curTimeUp > millis()) {
    curTimeUp = millis();
    Serial.println("Overflov");
  }
}
