#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <TimeLib.h>
#include <PubSubClient.h>
/*
   понедельник это 2
*/
/////////////Вафля///////////////////////////////////////////////////////////////
const char* ssid = "";  //  your network SSID (name)
const char* password = "";       // your network password

/////////////Синхронизация времени/////////////////////////////////////////////////
unsigned int localPort = 2390;      // local port to listen for UDP packets
IPAddress timeServerIP; // time.nist.gov NTP server address
const char* ntpServerName = "1.europe.pool.ntp.org";
const int timeZone = 8;
const int NTP_PACKET_SIZE = 48; // NTP time stamp is in the first 48 bytes of the message
byte packetBuffer[ NTP_PACKET_SIZE]; //buffer to hold incoming and outgoing packets
WiFiUDP Udp; // A UDP instance to let us send and receive packets over UDP

//////////Настройки MQTT//////////////////////////////////////////////////////////
const char *mqtt_server = ""; // Имя сервера MQTT
const int mqtt_port = 18432; // Порт для подключения к серверу MQTT
const char *mqtt_user = ""; // Логи для подключения к серверу MQTT
const char *mqtt_pass = ""; // Пароль для подключения к серверу MQTT

WiFiClient wclient;
PubSubClient client(wclient);

/////////Сигнатуры///////////////////////////////////////////////////////////

///////Переменные/////////////////////////////////////////////////////////////
uint64_t curTime = 0; //для организации циклов прерывания
uint64_t curTimeSync = 0; //для организации циклов прерывания
uint64_t lastReconnectAttempt = 0;
//uint32_t intervalSync = 3600000; //1 час
//const int interval = 5000; //основной цикл
uint32_t intervalSync = 60000; //1 час
uint32_t interval = 5000; //основной цикл
bool debug = true;
//////Входа//////////////////////////////////////////////////////////////////////

bool wifi_reconnect(byte trycount) {
  if (debug)Serial.print("TryConnect");
  byte i = 0;
  while (i < trycount && !wifi_status()) {
    delay(1000);
    Serial.print(".");
    i++;
  }
  if (wifi_status()) return true;
  else return false;
}

bool wifi_status() {
  if (WiFi.status() == WL_CONNECTED) return true;
  else return false;
}

//синхронизируем время через daySync дней
void timeSync() {
  if (debug)Serial.println(timeStatus());
  if (timeStatus() == 0) {
    if (wifi_status()) {
      if (debug)Serial.println("timeNotSet");
      setSyncProvider(getNtpTime);
    }
  } else {
    if (debug)Serial.println("timeSet");
  }
}

void digitalClockDisplay()
{
  // digital clock display of the time
  Serial.print(hour());
  printDigits(minute());
  printDigits(second());
  Serial.print(" ");
  Serial.print(day());
  Serial.print(".");
  Serial.print(month());
  Serial.print(".");
  Serial.print(year());
  Serial.print(" weekday= ");
  Serial.print(weekday());
  Serial.println();
}

void printDigits(int digits)
{
  Serial.print(":");
  if (digits < 10)
    Serial.print('0');
  Serial.print(digits);
}

void wifi_update() {
  if (debug) Serial.print("Wifi_update");
  if (!wifi_status()) {
    byte i = 0;
    Serial.print("Connecting to ");
    Serial.println(ssid);
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password);
    //пробуем подконектится 10 раз с интервалом в 3 сек.
    while (i < 4 && !wifi_status()) {
      Serial.print("_");
      Serial.print(i);
      delay(3000);
      i++;
    }
    Serial.println();
    if (wifi_status()) {
      Serial.println("WiFi connected");
      Serial.println("IP address: ");
      Serial.println(WiFi.localIP());
    } else Serial.println("Sync stopped, wifi is not found");
  }   else if (debug) Serial.print("..ok");
  if (debug) Serial.println("");
}

// send an NTP request to the time server at the given address
void sendNTPpacket(IPAddress & address)
{
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12] = 49;
  packetBuffer[13] = 0x4E;
  packetBuffer[14] = 49;
  packetBuffer[15] = 52;
  Udp.beginPacket(address, 123); //NTP requests are to port 123
  Udp.write(packetBuffer, NTP_PACKET_SIZE);
  Udp.endPacket();
}

time_t getNtpTime()
{
  IPAddress ntpServerIP;
  while (Udp.parsePacket() > 0) ;
  Serial.println("Transmit NTP Request");
  Udp.begin(localPort);
  WiFi.hostByName(ntpServerName, ntpServerIP);
  Serial.print(ntpServerName);
  Serial.print(": ");
  Serial.println(ntpServerIP);
  sendNTPpacket(ntpServerIP);
  uint32_t beginWait = millis();
  while (millis() - beginWait < 1500) {
    int size = Udp.parsePacket();
    if (size >= NTP_PACKET_SIZE) {
      Serial.println("Receive NTP Response");
      Udp.read(packetBuffer, NTP_PACKET_SIZE);
      unsigned long secsSince1900;
      secsSince1900 =  (unsigned long)packetBuffer[40] << 24;
      secsSince1900 |= (unsigned long)packetBuffer[41] << 16;
      secsSince1900 |= (unsigned long)packetBuffer[42] << 8;
      secsSince1900 |= (unsigned long)packetBuffer[43];
      return secsSince1900 - 2208988800UL + timeZone * SECS_PER_HOUR;
      Udp.flush();
      Udp.stop();
    }
  }
  Serial.println("No NTP Response :-(");
  return 0;
}

bool reconnect() {
  if (debug) Serial.println("Reconnect");
  if (client.connect("ESP_Nixie", mqtt_user, mqtt_pass)) {
    if (debug) Serial.println("MQQT Connection UP");
    /*
        client.subscribe("pump/manual");
        client.subscribe("pump/timeav");
        client.subscribe("pump/timebr");
        client.subscribe("pump/timelas");
        client.subscribe("pump/timeblab");
        client.subscribe("pump/setTime");
    */
  }
  return client.connected();
}

void mqqt_check() {
  if (debug) Serial.print("MQQT");
  if (!client.connected()) {
    if (millis() - lastReconnectAttempt > 5000) {
      if (debug) Serial.println(millis());
      lastReconnectAttempt = millis();
      if (reconnect()) {
        lastReconnectAttempt = 0;
      }
    }
    if (debug) Serial.print(" reconnect");
  } else {
    client.loop();
    if (debug) Serial.print(" loop");
  }
  if (debug) Serial.println("");
}

void callback(char* topic, byte* payload, unsigned int length) {
  String str;
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");

  for (int i = 0; i < length; i++) {
    if (debug) Serial.print((char)payload[i]);
    str += (char)payload[i];
  }
  if (debug) Serial.println();

  /*
    if ((String)topic == "pump/manual") {
      if (str == "all") {
        level_publish() ;
        lastdata_publish(now(), false);
        mqqt_check();
        pumping_all(av, br, las, blab, false);
      }
      if (str == "av")watering(valve_av, av);
      if (str == "br")watering(valve_br, br);
      if (str == "las") watering(valve_las, las);
      if (str == "blab")watering(valve_blab, blab);
    }
    /*адрес   что лежит
      0      av время полива авикулярий
      1      br время полива брах
      2      las время полива ласидоры
      3      blab время полива тараканов
  *//*
  if ((String)topic == "pump/timeav") {
  if (debug)Serial.println(str.toInt());
  if (writeEEPROM(str.toInt(), avadr))av = str.toInt();
  }
  if ((String)topic == "pump/timebr") {
  if (debug)Serial.println(str.toInt());
  if ( writeEEPROM(str.toInt(), bradr))br = str.toInt();
  }
  if ((String)topic == "pump/timelas") {
  if (debug)Serial.println(str.toInt());
  if ( writeEEPROM(str.toInt(), lasadr))las = str.toInt();
  }
  if ((String)topic == "pump/timeblab") {
  if (debug)Serial.println(str.toInt());
  if ( writeEEPROM(str.toInt(), blabadr))blab = str.toInt();
  }
  if ((String)topic == "pump/setTime") {
  //12.02/5
  if (debug) Serial.println(str);
  if (debug) Serial.println(str.length());
  if (str.length() > 0 & str.length() < 8) {
  String str1;
  String str2;
  String str3;
  str1 = str.substring(0, 2);
  if (debug)Serial.println(str1);
  if (debug)Serial.println(str1.toInt());
  str2 = str.substring(3, 5);
  if (debug)Serial.println(str2);
  if (debug)Serial.println(str2.toInt());
  str3 = str.substring(6);
  if (debug)Serial.println(str3);
  if (debug)Serial.println(str3.toInt());
  if (str1.toInt() < 24 & str1.toInt()>0)
  if (str2.toInt()< 60 & str2.toInt()>0)
  if (str3.toInt()< 8 & str3.toInt()>0) {
  if (debug)Serial.println("command right");
  hourpump = str1.toInt();
  writeEEPROM(hourpump, pumpHadr);
  minpump = str2.toInt();
  writeEEPROM(minpump, pumpMadr);
  week = str3.toInt();
  writeEEPROM(week, weekadr);
  }
  }
  }
*/
}

void setup()
{
  Serial.begin(115200);

  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);

  wifi_update();
  mqqt_check();

  curTime = millis();
  curTimeSync = millis();

  // setSyncInterval(2592000);
  // setSyncProvider(getNtpTime);

  if (debug) Serial.println("Setup_end");
}

void loop()
{
  if (millis() > (curTime + interval)) {
    curTime = millis();
    if (debug)Serial.println("======================================================================");
    wifi_update();
    mqqt_check();
  } else if (curTime > millis()) {
    curTime = millis();
    Serial.println("Overflov");
  }

  if (millis() > (curTimeSync + intervalSync)) {
    curTimeSync = millis();
    if (debug)Serial.println("---------------------------------------------------------------------");
    //timeSync();
    digitalClockDisplay();
  } else if (curTimeSync > millis()) {
    curTimeSync = millis();
    Serial.println("Overflov");
  }
}
