#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include "thermistor.h"
#include "HardwareSerial.h"
#include "ACS712.h"
/*
  Код
  int8_t    | char      | от -128 до 127
  uint8_t   | byte, unsigned char | от 0 до 255
  int16_t   | int     | от -32768 до 32767
  uint16_t  | unsigned int, word  | от 0 до 65535
  int32_t   | long      | от  -2147483648 до 2147483647
  uint32_t  | unsigned long   | от 0 до 4294967295
*/

//-------------пины--------------
int power_status = 2;
int cooler = 3;
int charge_s = 4;
int charge = 5;
int ntc_1 = 0;
int ntc_2 = 1;
//---------Переменные------------
uint32_t curTime = 0; //переменная для основного цикла
uint32_t acTime = 0;//переменная для цикла перебоя питания
uint16_t upCicle = 200;//номальный цикл
uint8_t interrupt = 0; //счетчик для организации прерываний
uint16_t temp_p1; //первая температура
uint16_t temp_p2;//вторая температура
bool debug = true;
int cur = 0; //ток
bool needCharge = false; //флаг зарядки
bool fanEnabled = false; //включить охлаждение
const int hyst = 5; //величина гистирезиса для сист охлаждения
bool ac_status = false; //флаг отсутствия 220в
//uint32_t powe_loss = 0;
//---------инициализация обьектов-------
LiquidCrystal_I2C lcd(0x27, 16, 2);
THERMISTOR therm1(ntc_1,        // Analog pin
                  10000,          // Nominal resistance at 25 ºC
                  3950,           // thermistor's beta coefficient
                  10000);
THERMISTOR therm2(ntc_2,        // Analog pin
                  10000,          // Nominal resistance at 25 ºC
                  3950,           // thermistor's beta coefficient
                  10000);

ACS712 current(ACS712_30A, A2);

void lcd_preset() {
  lcd.init();
  lcd.backlight();
  /*lcd.setCursor(3, 0);
    lcd.print("Hello, world!");
    lcd.setCursor(2, 1);
    lcd.print("Ywrobot Arduino!");
    lcd.setCursor(0, 2);
    lcd.print("Arduino LCM IIC 2004");
    lcd.setCursor(2, 3);
    lcd.print("Power By Ec-yuan!");
  */
  lcd_body();
  //c=30a_220_fan=99
  //t1=70_t2=70_ch
}

void setup() {

  pinMode(power_status, INPUT);
  pinMode(cooler, OUTPUT);
  pinMode(charge_s, INPUT);
  pinMode(charge, OUTPUT);
  pinMode(ntc_1, INPUT);
  pinMode(ntc_2, INPUT);

  lcd_preset();
  Serial.begin(9600);

  //current.calibrate();

  curTime = millis();
}

void lcd_body() {
  lcd.setCursor(0, 0);
  lcd.print("Cur=");

  lcd.setCursor(10, 0);
  lcd.print("Fan=");

  lcd.setCursor(0, 1);
  lcd.print("T1=");

  lcd.setCursor(6, 1);
  lcd.print("T2=");
}

void lcd_temp(int temp1, int temp2) {
  lcd.setCursor(3, 1);
  lcd.print(temp1);

  lcd.setCursor(9, 1);
  lcd.print(temp2);
}

void lcd_cur(int curnt) {
  lcd.setCursor(4, 0);
  lcd.print(curnt);
}

void lcd_vol(bool vol_220) {
  lcd.setCursor(8, 0);
  if (vol_220) {
    lcd.print("220");
  } else {
    lcd.print("   ");
  }
}

void lcd_charge(bool chrg) {
  lcd.setCursor(12, 0);
  if (chrg) {
    lcd.print("ch");
  } else {
    lcd.print("  ");
  }
}

void lcd_fan(int speed_f) {
  lcd.setCursor(14, 0);
  if (speed_f = 0)lcd.print("of");
  else
    lcd.print(speed_f);
}

void ntc_read() {
  temp_p1 = therm1.read();
  if (debug)Serial.print("temp point1= ");
  if (debug)Serial.println(temp_p1);
  temp_p2 = therm2.read();
  if (debug)Serial.print("temp point2= ");
  if (debug)Serial.println(temp_p1);
}

void voltage_ctrl() {
  if (!digitalRead(power_status)) {
    acTime = millis();
    if (ac_status != 1) {
      // AC ON
      ac_status = 1;
      needCharge = false;
    }
  }

  if (!ac_status)lcd_vol(true);
  else
    lcd_vol(false);

  //если 220 есть в течении 1 сек то сбрасываем
  if (ac_status && millis() - acTime > 1000) {
    // AC OFF
    needCharge = true;
    ac_status = 0;
    acTime = 0;
  }
}

void current_ctrl() {
  cur = current.getCurrentDC();
  if (debug)Serial.print("cur= ");
  if (debug)Serial.println(cur);
  lcd_cur(cur);
}

void fan_ctrl() {
  byte fan_speed = 0;
  ntc_read();
  lcd_temp(temp_p1, temp_p2);

  if (!fanEnabled) {
    if (temp_p1 > 40 || temp_p2 > 40) {
      fanEnabled = true;
    }
  } else {
    if (temp_p1 < (40 - hyst) || temp_p2 < (40 - hyst)) {
      fanEnabled = false;
    }
  }

  if (fanEnabled) {
    if ((temp_p1 < 50 || temp_p2 < 50)) fan_speed = 64;
    if ((temp_p1 < 60 || temp_p2 < 60) & (temp_p1 > 50 || temp_p2 > 50)) fan_speed = 128;
    if ((temp_p1 < 70 || temp_p2 < 70) & (temp_p1 > 60 || temp_p2 > 60)) fan_speed = 255;
    if (debug)Serial.print("fan_speed= ");
    if (debug)Serial.println(fan_speed);
    analogWrite(cooler, fan_speed);
  } else {
    analogWrite(cooler, 0);
  }
  lcd_fan(fan_speed);
}

void charge_ctrl() {
  //если требуется зарядка
  if (needCharge) {
    //и она не включена
    if (digitalRead(charge_s)) {
      //значит зарядка не запущена если здесь +
      digitalWrite(charge, HIGH);
      lcd_charge(true);
      if (debug)Serial.println("Charge RUN");
    } else {
      //+ нет значит идет зарядка
      needCharge = false;
      digitalWrite(charge, LOW);
      if (debug)Serial.println("Charging");
    }
  } if (!digitalRead(charge_s)) {
    //+ нет значит зарядка идет сейчас
    lcd_charge(true);
  } else {
    lcd_charge(false);
  }
}

void DoSomeThing() {
  switch (interrupt) {
    case 5://1000
      current_ctrl();
      break;
    case 10://2000
      break;
    case 15://3000
      fan_ctrl();
      charge_ctrl();
      break;
  }
}

void loop() {
  if (millis() >= (curTime + upCicle)) {
    curTime = millis();
    interrupt++;
    if (interrupt > 16) interrupt = 0;
    voltage_ctrl();
    DoSomeThing();
  } else if (curTime > millis()) {
    curTime = millis();
    if (debug)Serial.println("Overflow");
  }
}

/*
 * void loop() {
  if (millis() >= (curTime + upCicle)) {
    curTime = millis();
    interrupt++;
    if (interrupt > 16) interrupt = 0;
    voltage_ctrl();
    DoSomeThing();
  } else if (curTime > millis()) {
    curTime = millis();
    if (debug)Serial.println("Overflow");
  }
}
 */
