#include <Adafruit_Si7021.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <PubSubClient.h>
#include <Wire.h>
/*
   понедельник это 2
*/
/////////////Вафля///////////////////////////////////////////////////////////////
const char* ssid = "Box";
const char* password = "heinztockler";
/*
  const char* ssid = "Box";
  const char* password = "heinztockler";
*/
//////////Настройки MQTT//////////////////////////////////////////////////////////
const char *mqtt_server = "192.168.0.4"; // Имя сервера MQTT
const int mqtt_port = 1883; // Порт для подключения к серверу MQTT
const char *mqtt_user = "heinz_box"; // Логи для подключения к серверу MQTT
const char *mqtt_pass = "heinz192422901"; // Пароль для подключения к серверу MQTT

WiFiClient wclient;
PubSubClient client(wclient);
Adafruit_Si7021 Hum = Adafruit_Si7021();

/////////Сигнатуры///////////////////////////////////////////////////////////

///////Переменные/////////////////////////////////////////////////////////////
uint64_t curTime = 0; //для организации циклов прерывания
uint64_t curTimeSync = 0; //для организации циклов прерывания
uint64_t lastReconnectAttempt = 0;
uint32_t intervalSync = 5000; //
uint32_t interval = 1000; //основной цикл
bool debug = true;
float downTemp = 0;
float downHumid = 0;

//для приема сигналов
bool downLight = false; //включение света в подвале
bool downHeater = false; //включение подогрева в подвале
//bool manualHeat = false; //включение подогрева в подвале
//////Входа//////////////////////////////////////////////////////////////////////
/*
  uint8_t left = 4;//это sda
  uint8_t right = 5;//это scl

  uint8_t pir = 0;//adc
*/
uint8_t light = 14;
uint8_t heat = 12;
uint8_t button = 13;

bool wifi_status() {
  if (WiFi.status() == WL_CONNECTED) return true;
  else return false;
}

void wifi_update() {
  if (debug) Serial.print("Wifi_update");
  if (!wifi_status()) {
    byte i = 0;
    Serial.print("Connecting to ");
    Serial.println(ssid);
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password);
    //пробуем подконектится 10 раз с интервалом в 3 сек.
    while (i < 4 && !wifi_status()) {
      Serial.print("_");
      Serial.print(i);
      delay(3000);
      i++;
    }
    Serial.println();
    if (wifi_status()) {
      Serial.println("WiFi connected");
      Serial.println("IP address: ");
      Serial.println(WiFi.localIP());
    } else Serial.println("Sync stopped, wifi is not found");
  }   else if (debug) Serial.print("..ok");
  if (debug) Serial.println("");
}

bool reconnect() {
  if (debug) Serial.println("Reconnect");
  if (client.connect("ESP_Nixie", mqtt_user, mqtt_pass)) {
    if (debug) Serial.println("MQQT Connection UP");
    client.subscribe("box/downLight");
    client.subscribe("box/downHeater");
  }
  return client.connected();
}

void mqqt_check() {
  if (debug) Serial.print("MQQT");
  if (!client.connected()) {
    if (millis() - lastReconnectAttempt > 5000) {
      if (debug) Serial.println(millis());
      lastReconnectAttempt = millis();
      if (reconnect()) {
        lastReconnectAttempt = 0;
      }
    }
    if (debug) Serial.print(" reconnect");
  } else {
    client.loop();
    if (debug) Serial.print(" loop");
  }
  if (debug) Serial.println("");
}

void callback(char* topic, byte* payload, unsigned int length) {
  String str;
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");

  for (int i = 0; i < length; i++) {
    if (debug) Serial.print((char)payload[i]);
    str += (char)payload[i];
  }
  if (debug) Serial.println();

  if ((String)topic == "box/downLight"&str.length() < 6) {
    if (debug)Serial.println(str);
    if (str == "true")downLight = true;
    if (str == "false")downLight = false;
    if (debug)Serial.println("downLight arrived");
  }

  if ((String)topic == "box/downHeater"&str.length() < 6) {
    if (debug)Serial.println(str);
    if (str == "true")downHeater = true;
    if (str == "false") downHeater = false;
    //  manualHeat = downHeater;
    if (debug)Serial.println("downHeater arrived");
  }
}

void temp_status(float val) {
  char message[8]; //сюда кэшируем значение
  String str;
  str += val;
  str.toCharArray(message, 8);
  client.publish("box/downT", message);
}

void humid_status(float val) {
  char message[8]; //сюда кэшируем значение
  String str;
  str += val;
  str.toCharArray(message, 8);
  client.publish("box/downH", message);
}

void lightCtr() {
  if (downLight) {
    digitalWrite(light, HIGH);
  } else {
    digitalWrite(light, LOW);
  }
}

void heaterCtrl() {

  if (downHeater) {
    digitalWrite(heat, HIGH);
  } else {
    if (downTemp <= 2) digitalWrite(heat, HIGH);
    else if (downTemp > 5)digitalWrite(heat, LOW);
  }
}

void parametrPublic() {
  downHumid = Hum.readHumidity();
  downTemp = Hum.readTemperature();
  if (debug)Serial.print("Humidity:   ");
  if (debug) Serial.print(downHumid, 2);
  if (debug) Serial.print("\tTemperature: ");
  if (debug) Serial.println(downTemp, 2);
  temp_status(downTemp);
  humid_status(downHumid);
}

void button_chek() {
  if (digitalRead(button)) {
    downLight = true;
  } else {
    downLight = false;
  }
}

void setup()
{
  Serial.begin(115200);

  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);

  pinMode(light, OUTPUT);
  pinMode(heat, OUTPUT);
  pinMode(button, INPUT);
  Hum.begin();

  wifi_update();
  mqqt_check();

  curTime = millis();
  curTimeSync = millis();

  if (debug) Serial.println("Setup_end");
}

void loop()
{
  if (millis() > (curTime + interval)) {
    curTime = millis();
    if (debug)Serial.println("======================================================================");
    wifi_update();
    mqqt_check();
    lightCtr();
    heaterCtrl();
  } else if (curTime > millis()) {
    curTime = millis();
    Serial.println("Overflov");
  }

  if (millis() > (curTimeSync + intervalSync)) {
    curTimeSync = millis();
    if (debug)Serial.println("---------------------------------------------------------------------");
    parametrPublic();
  } else if (curTimeSync > millis()) {
    curTimeSync = millis();
    Serial.println("Overflov");
  }
}
