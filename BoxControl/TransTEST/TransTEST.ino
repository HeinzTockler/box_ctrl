#include <EmonLib.h>

EnergyMonitor emon1;
int acp = 0;
//для корректного измерения необходимо через трансформатор тока пропустить 1 виток провода
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(A0, INPUT);
  //калибровочный коэфициент можно посчитать так
  //https://learn.openenergymonitor.org/electricity-monitoring/ctac/ct-and-ac-power-adaptor-installation-and-calibration-theory?redirected=true
  emon1.current(0, 1);
  Serial.begin(9600);
}

// the loop function runs over and over again forever
void loop() {
  acp = analogRead(A0);
  Serial.println(acp);
  double Irms = emon1.calcIrms(1480);
  Serial.println(Irms);
  delay(500);                       // wait for a second
}
