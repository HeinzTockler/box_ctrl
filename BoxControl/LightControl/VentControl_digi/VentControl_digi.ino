#include <VirtualWire.h>
#include <EasyTransferVirtualWire.h>
//http://digistump.com/package_digistump_index.json
unsigned int curTime = 0;//прерывание по времени
bool flag = false;
byte relay = 2;//реле
byte led = 1;
byte transmit = 0;
char mess[5];
String str;

EasyTransferVirtualWire ET;

struct SEND_DATA_STRUCTURE {
  //put your variable definitions here for the data you want to send
  //THIS MUST BE EXACTLY THE SAME ON THE OTHER ARDUINO
  //Struct can'e be bigger then 26 bytes for VirtualWire version
  int id;
  int command;
};

//give a name to the group of data
SEND_DATA_STRUCTURE mydata;

void setup() { 
 // ET.begin(details(mydata));
  // put your setup code here, to run once:
  pinMode(relay, OUTPUT);
  pinMode(led, OUTPUT);
  vw_set_rx_pin(transmit);
  vw_setup(2000);   // Bits per se
  vw_set_ptt_inverted(true); // Required for DR3100
  vw_rx_start();       // Start the receiver PLL running
  pinMode(transmit, INPUT);
  led_blink(2);
 
  //curTime = millis();
}

void loop() {
  /*
    uint8_t buf[VW_MAX_MESSAGE_LEN];
    uint8_t buflen = VW_MAX_MESSAGE_LEN;

    if (vw_get_message(buf, &buflen)) // Non-blocking
    {
    // mess = '';
    str = "";
    for (int i = 0; i < buflen; i++)
    {
      mess[i] = (char)buf[i];

      led_blink(1);
    }
    delay(100);
    str.toCharArray(mess, buflen);
    /*
      if (mess == 'n') {
      digitalWrite(relay, HIGH);
      led_blink(1);
      delay(100);

      }
      if (mess == 'f') {
      digitalWrite(relay, LOW);
      led_blink(2);
      delay(100);
      }*/
  /*
      if (str == "n1") {
        digitalWrite(relay, HIGH);
        led_blink(3);
        delay(500);
        digitalWrite(relay, LOW);
      }
      if (str == "f") {
        led_blink(1);
        digitalWrite(relay, LOW);
      }
    }
    //mess='';
    // str = "";
    /*
      digitalWrite(relay, HIGH);
      delay(4000);
      digitalWrite(relay, LOW);
      delay(4000);
  */
  if (ET.receiveData()) {
    led_blink(1);
    if (mydata.id == 101) {
      led_blink(2);
      if (mydata.command == 1020) {
        digitalWrite(relay, HIGH);
        led_blink(3);
        delay(100);
      }

      if (mydata.command == 1021) {
        digitalWrite(relay, LOW);
        led_blink(3);
        delay(100);
      }
    }
  }
}

void led_blink(byte count) {
  for (int i = 1 ; i <= count; i++) {
    digitalWrite(led, 1);
    delay(300);
    digitalWrite(led, 0);
    delay(300);
  }
}

/*
  byte relay = 2;//реле
  char *controller;
  char *mess;
  byte transmit = 0;

  void setup() {
  vw_set_ptt_inverted(true); //
  vw_set_tx_pin(transmit);
  vw_setup(2000);// скорость передачи данных в Kbps
  pinMode(transmit, OUTPUT);
  }

  void loop() {
  //controller = "1" ;
  mess = "on";
  vw_send((uint8_t *)mess, strlen(mess));
  vw_wait_tx(); // ждем, пока отправится все сообщение
  digitalWrite(1, 1);
  delay(4000);
  mess = "off";
  // controller = "0";
  vw_send((uint8_t *)mess, strlen(mess));
  vw_wait_tx(); // ждем, пока отправится все сообщение
  digitalWrite(1, 0);
  delay(2000);
  }
*/
